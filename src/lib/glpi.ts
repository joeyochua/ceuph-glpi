import type { Handle } from "@sveltejs/kit";
import GlpiApi from 'glpi-api';
import { env } from "$env/dynamic/private";

export const handle = (async ({ event, resolve }) => {
    const glpi = new GlpiApi({
        apiurl: env.GLPI_API_URL,
        app_token: env.GLPI_APP_TOKEN,
        user_token: env.GLPI_USER_TOKEN,
    });
    await glpi.initSession();
    event.locals.glpi = glpi;
    const response = await resolve(event);
    await glpi.killSession();
    return response;
}) satisfies Handle;

export default handle;