type ItemType = 'Computer' | 'Infocom' | 'Item_DeviceGeneric' | 'Item_Ticket' | 'Ticket' | 'Location';
type LogicalOperator = 'AND' | 'OR' | 'AND NOT' | 'AND NOT';
type SearchType = 'contains' | 'equals' | 'notequals' | 'lessthan' | 'morethan' | 'under' | 'notunder';

type Criteria = {
    link: LogicalOperator;
    field: number;
    searchtype: SearchType;
    value: string | number;
};
type MetaCriteria = {
    _link_: LogicalOperator;
    _itemtype_: ItemType;
    _field_: number;
    _searchtype_: SearchType;
    _value_: string | number;
}
type SearchOptions = {
    criteria?: Array<Criteria>,
    metacriteria?: Array<any>,
    sort?: string,
    order?: 'ASC' | 'DESC',
    forcedisplay?: Array<number>,
    rawdata?: boolean,
    withindexes?: boolean,
    uid_cols?: boolean,
    giveItems?: boolean,
}
type ResultMultiple = {
    totalCount: number;
    count: number;
    sort: Array<number>;
    order: Array<'ASC' | 'DESC'>;
    data: Record<string, any>;
    'content-range': string;
}
type ResultSingle = {
    code: number;
    data: Record<string, any>;
};
declare module 'glpi-api' {
    export default class GlpiApi {
        constructor(config: {
            apiurl: string;
            app_token: string;
            user_token?: string;
            auth?: {
                username: string;
                passwrod: string;
            }
        });
        initSession(): Promise<GlpiSession>;
        killSession(): Promise<any>;
        getItem(itemType: ItemType, id: number): Promise<ResultSingle>;
        getSubItems(itemType: ItemType, id: number, subItemType: ItemType): Promise<ResultSingle>;
        search(itemType: ItemType, searchOptions: SearchOptions): Promise<ResultMultiple>;
    }
}