import { sequence } from "@sveltejs/kit/hooks";
import glpi from "$lib/glpi";

export const handle = sequence(glpi);