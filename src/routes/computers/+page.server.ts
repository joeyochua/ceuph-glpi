import type { PageServerLoad } from './$types';

export const load = (async ({ locals, url }) => {
    let criteria: Array<Criteria> = [];
    const computerName = url.searchParams.get('computerName');
    if (computerName) {
        criteria.push({
            link: 'AND',
            field: 1,
            searchtype: 'contains',
            value: computerName
        });
    }
    const computers = locals.glpi.search('Computer', {
        criteria,
    });
    return { computers };
}) satisfies PageServerLoad;