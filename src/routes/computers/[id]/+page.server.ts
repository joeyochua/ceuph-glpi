import type { PageServerLoad } from './$types';

export const load = (async ({ params, locals, url }) => {
    const { id } = params;
    const computer = await locals.glpi.getItem('Computer', id);
    const location = await locals.glpi.getItem('Location', computer.data.locations_id);
    const manufacturer = await locals.glpi.getItem('Manufacturer', computer.data.manufacturers_id);
    const model = await locals.glpi.getItem('ComputerModel', computer.data.computermodels_id);
    const infocom = await locals.glpi.getSubItems('Computer', id, 'Infocom');
    let tickets = await getTickets(id, locals);
    const selectedTickets = url.searchParams.getAll('tickets[]');
    if (selectedTickets.length > 0) {
        tickets = tickets.filter(ticket => selectedTickets.includes(ticket.data.id.toString()));
    }
    return { computer, location, manufacturer, model, infocom, tickets };
}) satisfies PageServerLoad;

const getTickets = (async (id: string, locals: App.Locals) => {
    const itemTickets = await locals.glpi.getSubItems('Computer', id, 'Item_Ticket');
    const tickets = [];
    for (let item of itemTickets.data) {
        const ticket = await locals.glpi.getItem('Ticket', item.tickets_id);
        const users = await locals.glpi.getSubItems('Ticket', item.tickets_id, 'Ticket_User');
        ticket.data.users = [];
        for (let user of users.data) {
            if (user.type == 2) {
                const userItem = await locals.glpi.getItem('User', user.users_id);
                ticket.data.users.push(userItem.data);
            }
        }
        tickets.push(ticket);
    }
    return tickets;
});